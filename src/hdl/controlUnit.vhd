-------------------------------------------------------------------------------
-- Title      : controlUnit
-- Project    :
-------------------------------------------------------------------------------
-- File       : operativeUnit.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2018-04-11
-- Last update: 2019-02-13
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Control unit of a sequential FIR filter.
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-04-11  1.0      jnbazin Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity controlUnit is

  port (
    I_clock               : in  std_logic;  -- global clock
    I_reset               : in  std_logic;  -- asynchronous global reset
    I_inputSampleValid    : in  std_logic;  -- Control signal to load the input sample in the sample shift register and shift the register
    I_processingDone      : in  std_logic;
    O_loadShift           : out std_logic;  -- filtered sample
    O_initAddress         : out std_logic;  -- Control signal to initialize register read address
    O_incrAddress         : out std_logic;  -- Control signal to increment register read address
    O_initSum             : out std_logic;  -- Control signal to initialize the MAC register
    O_loadSum             : out std_logic;  -- Control signal to load the MAC register;
    O_loadY               : out std_logic;  -- Control signal to load Y register
    O_FilteredSampleValid : out std_logic  -- Data valid signal for filtered sample
    );

end entity controlUnit;
architecture archi_operativeUnit of controlUnit is


  type T_state is (WAIT_SAMPLE, STORE, PROCESSING_LOOP, OUTPUT, WAIT_END_SAMPLE);  -- state list
  signal SR_presentState : T_state;
  signal SR_futurState   : T_state;

begin

  process (_BLANK_) is
  begin
    if I_reset = '1' then               -- asynchronous reset (active high)
      SR_presentState <= _BLANK_
    elsif rising_edge(I_clock) then     -- rising clock edge
      _BLANK_
    end if;
  end process;

  process (_BLANK_) is
  begin
    case SR_presentState is

      when WAIT_SAMPLE =>
        _BLANK_

      when others => null;
    end case;
  end process;

  O_loadShift           <= '1' when _BLANK_ ;
  O_initAddress         <= '1' when _BLANK_ ;
  O_incrAddress         <= '1' when _BLANK_ ;
  O_initSum             <= '1' when _BLANK_ ;
  O_loadSum             <= '1' when _BLANK_ ;
  O_loadY               <= '1' when _BLANK_ ;
  O_FilteredSampleValid <= '1' when _BLANK_ ;





end architecture archi_operativeUnit;
