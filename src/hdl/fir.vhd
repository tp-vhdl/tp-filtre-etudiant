library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fir is

  generic (
    dwidth : natural := 18;
    ntaps  : natural := 15);

  port (
    din          : in  std_logic_vector(dwidth-1 downto 0);
    dout         : out std_logic_vector(dwidth-1 downto 0);
    config_sw    : in  std_logic_vector(4 downto 0);  --inutilise dans le TP majeure
    clk          : in  std_logic;
    rst          : in  std_logic;
    ce           : in  std_logic;  -- signal de validation de din a la frequence des echantillons audio
    dbg_output_0 : out std_logic_vector(7 downto 0);  --inutilise dans le TP majeure
    dbg_output_1 : out std_logic_vector(7 downto 0);  --inutilise dans le TP majeure
    dbg_output_2 : out std_logic;       --inutilise dans le TP majeure
    dbg_output_3 : out std_logic;       --inutilise dans le TP majeure
    dbg_output_4 : out std_logic       --inutilise dans le TP majeure
--    dout_valid   : out std_logic
    );

end fir;

architecture myarch of fir is

  component firUnit is
    port (
      I_clock               : in  std_logic;
      I_reset               : in  std_logic;
      I_inputSample         : in  std_logic_vector(7 downto 0);
      I_inputSampleValid    : in  std_logic;
      O_filteredSample      : out std_logic_vector(7 downto 0);
      O_filteredSampleValid : out std_logic);
  end component firUnit;


  signal D_in, D_out : std_logic_vector(7 downto 0);

begin  -- myarch

-- Quantization on 8 bits or less

-- When config_sw(3)='1', rounding is made by finding the nearest value else rounding is made by truncating.
  prc : process (config_sw(3 downto 0), din) is
  begin  -- process prc
    case to_integer(unsigned(config_sw(3 downto 0))) is
      when 0      => D_in <= din(dwidth-1 downto dwidth -8);
      when 1      => D_in <= din(dwidth-1 downto dwidth -7)&'0';
      when 2      => D_in <= din(dwidth-1 downto dwidth -6)&"00";
      when 3      => D_in <= din(dwidth-1 downto dwidth -5)&"000";
      when 4      => D_in <= din(dwidth-1 downto dwidth -4)&"0000";
      when 5      => D_in <= din(dwidth-1 downto dwidth -3)&"00000";
      when 6      => D_in <= din(dwidth-1 downto dwidth -2)&"000000"; 
      when 7      => D_in <= din(dwidth-1)&"0000000";
      when 8      => if din(dwidth-8) = '0' then D_in <= din(dwidth-1 downto dwidth -8);else D_in <=std_logic_vector(signed(din(dwidth-1 downto dwidth -8))+1); end if;
      when 9      => if din(dwidth-8) = '0' then D_in <= din(dwidth-1 downto dwidth -7)&'0'; else D_in <=std_logic_vector(signed(din(dwidth-1 downto dwidth -7))+1)&'0'; end if;
      when 10      => if din(dwidth-7) = '0' then D_in <= din(dwidth-1 downto dwidth -6)&"00"; else D_in <=std_logic_vector(signed(din(dwidth-1 downto dwidth -6))+1)&"00"; end if;
      when 11      => if din(dwidth-6) = '0' then D_in <= din(dwidth-1 downto dwidth -5)&"000"; else D_in <=std_logic_vector(signed(din(dwidth-1 downto dwidth -5))+1)&"000"; end if;
      when 12      => if din(dwidth-5) = '0' then D_in <= din(dwidth-1 downto dwidth -4)&"0000"; else D_in <=std_logic_vector(signed(din(dwidth-1 downto dwidth -4))+1)&"0000"; end if;
      when 13      => if din(dwidth-4) = '0' then D_in <= din(dwidth-1 downto dwidth -3)&"00000"; else D_in <=std_logic_vector(signed(din(dwidth-1 downto dwidth -3))+1)&"00000"; end if;
      when 14      => if din(dwidth-3) = '0' then D_in <= din(dwidth-1 downto dwidth -2)&"000000"; else D_in <=std_logic_vector(signed(din(dwidth-1 downto dwidth -2))+1)&"000000"; end if;
      when 15      => D_in <= din(dwidth-1)&"0000000";
      when others => D_in <= (others => '0');
    end case;
  end process prc;
  
--FIR over 8 bits

  firUnit_1 : entity work.firUnit
    port map (
      I_clock               => clk,
      I_reset               => rst,
      I_inputSample         => D_in,
      I_inputSampleValid    => ce,
      O_filteredSample      => D_out,
      O_filteredSampleValid => open);


-- End of FIR


  dout(dwidth-1 downto dwidth -8) <= D_out when config_sw(4) = '1' else D_in;
  dout(dwidth-9 downto 0)         <= (others => '0');





end myarch;
