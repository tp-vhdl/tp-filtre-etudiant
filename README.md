# TP filtre audio

[ ] Comment utiliser Git et Gitlab : [https://tp-vhdl.gitlab-pages.imt-atlantique.fr/gitlab/](https://tp-vhdl.gitlab-pages.imt-atlantique.fr/gitlab/)
[ ] Énoncé du TP : [https://tp-vhdl.gitlab-pages.imt-atlantique.fr/filtre/](https://tp-vhdl.gitlab-pages.imt-atlantique.fr/filtre/)

## Questions

Les réponses aux questions du sujet doivent être complétées dans le fichier markdown `docs/compte-rendu.md` et le diagramme de la FSM `doc/img/FSM.drawio` modifié grâce à l'outil en ligne [https://app.diagrams.net/](Draw.io), en choisissant l'enregistrement sur périphérique. Il faut ensuite l'exporter au format PNG en écrasant le fichier PNG existant.
