# Compte rendu du TP Filtre audio

## Diagramme de la FSM

![Diagramme de la FSM](./img/FSM.png)


## Questions

### Question filtre 1 : Combien de processus sont utilisés et de quelles natures sont-ils ? Comment les différenciez-vous ?


### Question filtre 2 : La simulation vous permet-elle de valider votre description VHDL ? Justifiez.


### Question filtre 3 : Validez-vous la conception de l’unité de contrôle ?


### Question filtre 4 : Combien de processus sont utilisés et de quelles natures sont-ils ?


### Question filtre 5 : La simulation vous permet-elle de valider votre description VHDL ? Sinon, quel élément pose problème ? Comment pouvez-vous le corriger ? Justifiez


### Question filtre 6 : Validez-vous la conception de l’unité opérative ? Sinon, quel élément pose problème ? Comment pouvez-vous le corriger ?
